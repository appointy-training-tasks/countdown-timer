import React, { Component } from "react";
import "./Card.css";

interface Props {
  displayValue: number;
  displayText: string;
}

export default class Card extends Component<Props> {
  shouldComponentUpdate(nextProps: Props): boolean {
    return nextProps.displayValue !== this.props.displayValue;
  }

  render() {
    return (
      <div className="card">
        <div className="card__value">{this.props.displayValue}</div>
        <div className="card__text">{this.props.displayText}</div>
      </div>
    );
  }
}

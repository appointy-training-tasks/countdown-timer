import React, { Component } from "react";
import "./Navbar.css";

interface Props {
  onClearHandler: React.MouseEventHandler;
}

export default class Navbar extends Component<Props> {
  render() {
    return (
      <nav className="navbar">
        <div className="navbar__title">
          <h1>Countdown Timer</h1>
        </div>
        <div className="navbar__buttons">
          <button
            className="navbar__button"
            onClick={this.props.onClearHandler}>
            Clear
          </button>
          <button className="navbar__button">Settings</button>
        </div>
      </nav>
    );
  }
}

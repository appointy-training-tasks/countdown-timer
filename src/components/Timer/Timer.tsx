import React, { Component } from "react";
import Card from "../Card/Card";
import "./Timer.css";

interface Props {
  targetDate: Date | null;
}

interface State {
  secondsLeft: number;
}

export default class Timer extends Component<Props, State> {
  timerId!: NodeJS.Timer;

  constructor(props: Props) {
    super(props);

    if (this.props.targetDate) {
      const deltaTime = Math.floor(
        (this.props.targetDate.getTime() - Date.now()) / 1000
      );

      this.state = {
        secondsLeft: Math.max(deltaTime, 0),
      };
    } else {
      this.state = {
        secondsLeft: 0,
      };
    }

    this.tick = this.tick.bind(this);
    this.timerId = setInterval(this.tick, 1000);
  }

  tick() {
    if (this.state.secondsLeft <= 0) return;

    this.setState((state) => {
      return { secondsLeft: state.secondsLeft - 1 };
    });
  }

  shouldComponentUpdate(nextProps: Props, nextState: State): boolean {
    if (nextProps.targetDate === null && this.state.secondsLeft > 0) {
      this.setState({ secondsLeft: 0 });
    }

    return this.state.secondsLeft > nextState.secondsLeft;
  }

  componentDidUpdate() {
    this.timerId = setInterval(() => this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
  }

  render() {
    return (
      <div className="timer">
        <div className="timer__heading">Countdown ends in...</div>
        <div className="timer__cards">
          <Card
            displayValue={Math.floor(this.state.secondsLeft / 86400)}
            displayText={"days"}
          />
          <Card
            displayValue={Math.floor(this.state.secondsLeft / 3600) % 24}
            displayText={"hours"}
          />
          <Card
            displayValue={Math.floor(this.state.secondsLeft / 60) % 60}
            displayText={"minutes"}
          />
          <Card
            displayValue={this.state.secondsLeft % 60}
            displayText={"seconds"}
          />
        </div>
      </div>
    );
  }
}

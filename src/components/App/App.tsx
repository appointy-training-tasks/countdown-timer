import React, { Component } from "react";
import Navbar from "../Navbar/Navbar";
import Timer from "../Timer/Timer";

import "./App.css";

interface Props {}
interface State {
  targetDate: Date | null;
}

export default class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.onClearHandler = this.onClearHandler.bind(this);

    this.state = { targetDate: new Date("June 11, 2022 00:00:00") };
  }

  onClearHandler() {
    this.setState({ targetDate: null });
  }

  render() {
    return (
      <div className="app">
        <Navbar onClearHandler={this.onClearHandler} />
        <Timer targetDate={this.state.targetDate} />
      </div>
    );
  }
}

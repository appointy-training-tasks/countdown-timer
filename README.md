# Countdown Timer

## Introduction

This is a project by [Pranav Balaji](https://gitlab.com/PranavBalaji) to design a countdown-timer using class-based components in ReactJS.
The project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Packages and Specifications

1. react@^18.1.0
2. typescript@^4.7.2

## Running locally

<code>npm start</code> runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser. The page will reload if you make edits. You will also see any lint errors in the console.

## Screenshot(s)

![screenshot](./assets/screenshot.PNG)

## References

[React documentation](https://reactjs.org/)
